

A text turn-based strategy game that you have to finish in 26 turns.
Created for Java jam.

If you run it by gradle:

`./gradlew run`

Or download the jar from [Release page](https://gitlab.com/szatkus/dzem26/-/releases)