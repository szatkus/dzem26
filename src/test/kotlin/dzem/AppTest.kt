package dzem

import kotlin.test.Test

class AppTest {
    @Test fun testPlay() {
        val TESTS = 5000000
        val counts = intArrayOf(0, 0, 0, 0, 0, 0, 0)
        val levels = intArrayOf(7, 11, 15, 19, 22, 24, 26)

        for (i in 1..TESTS) {
            val game = Game()
            /*game.turn = 12
            game.buildings = mutableListOf(House, Farm, Mine, Barracks, House, Barracks, Barracks, Farm)
            game.units = mutableListOf(Lumberjack, Farmer, Miner, Worker, Worker, Worker, Worker, Worker, Militia, Worker)*/
            while (!game.isFinished()) {
                do {
                    game.clear()
                    val buildings = game.getAvailableBuildings().plus(null as Building?)
                    buildings.shuffled().first()?.let { game.build(it) }

                    val units = game.getAvailableUnits().plus(null as Creature?)
                    if (game.getWorkers().isNotEmpty()) {
                        units.shuffled().first()?.let { game.convertTo(it) }
                    }
                } while(!game.canAfford())

                game.nextTurn()
                for ((index, turn) in levels.withIndex()) {
                    if (game.turn == turn) {
                        counts[index]++
                    }
                }
            }

        }
        for (c in counts) {
            println(c)
        }
    }
}
