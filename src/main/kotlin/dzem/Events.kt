package dzem

sealed class Event
class AttackEvent(val attackers: List<Creature>): Event()
class DeathEvent(val gone: Creature): Event()
object WinEvent: Event()
object LoseEvent: Event()
object WorkerEvent: Event()
class ConvertEvent(val creature: Creature): Event()
class BuildEvent(val building: Building): Event()
class ResourcesEvent(val resources: Resources): Event()