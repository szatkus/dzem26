package dzem

class Fighter(val creature: Creature, var damage: Int = 0)




class Game {
  var turn = 0
  var toBuild: Building? = null
  var toConvert: Creature? = null
  var buildings: MutableList<Building> = ArrayList()
  var units: MutableList<Creature> = ArrayList()
  var resources = Resources(
          wood = 3,
          iron = 3,
          food = 3
  )
  var defeated = false
  var observer: (Event) -> Unit = {}

  fun clear() {
    toBuild = null
    toConvert = null
  }

  fun isFinished(): Boolean = defeated || turn > 25

  fun getAvailableBuildings(): Set<Building> {
    val availableBuildings = mutableSetOf<Building>(House)
    if (buildings.contains(House)) {
      availableBuildings.add(Farm)
      availableBuildings.add(Mine)
    }
    if (buildings.contains(Mine)) {
      availableBuildings.add(Farm)
      availableBuildings.add(Barracks)
    }
    if (buildings.contains(Barracks)) {
      availableBuildings.add(Shrine)
    }
    if (buildings.contains(Shrine)) {
      availableBuildings.add(Castle)
        availableBuildings.add(Tower)
    }
    if (buildings.contains(Castle)) {
      availableBuildings.add(Stable)
        availableBuildings.add(Wall)
    }
      availableBuildings.removeIf { b -> buildings.count {it == b} == b.quantity }
    return availableBuildings
  }

  fun build(building: Building) {
    toBuild = building
  }

  fun getAvailableUnits(): Set<Creature> {
    val availableUnits = mutableSetOf<Creature>()
    if (buildings.contains(Farm)) {
      availableUnits.add(Lumberjack)
    }
    if (buildings.contains(Farm)) {
      availableUnits.add(Farmer)
    }
    if (buildings.contains(Mine)) {
      availableUnits.add(Miner)
    }
    if (buildings.contains(Barracks)) {
      availableUnits.add(Militia)
      availableUnits.add(Guardian)
    }
    if (buildings.contains(Shrine)) {
      availableUnits.add(Mage)
    }
    if (buildings.contains(Castle)) {
      availableUnits.add(Warrior)
    }
    if (buildings.contains(Stable)) {
      availableUnits.add(Cavalry)
    }
    return availableUnits
  }

  fun getWorkers(): List<Worker> {
    return units.filterIsInstance(Worker::class.java)
  }

  fun convertTo(unit: Creature) {
    toConvert = unit
  }

  fun canAfford(): Boolean {
    val reducedResources = resources.reduce(toBuild?.cost).reduce(toConvert?.cost)
    return !reducedResources.isNegative()
  }

  fun nextTurn() {
    if (turn == 7) {
      fight(mutableListOf(Fighter(Slime)))
    }
    if (turn == 10) {
      fight(mutableListOf(Fighter(Slime), Fighter(Slime), Fighter(Slime)))
    }
    if (turn == 14) {
      fight(mutableListOf(Fighter(Goblin)))
    }
    if (turn == 18) {
        fight(mutableListOf(Fighter(Goblin), Fighter(Goblin), Fighter(Goblin)))
    }
    if (turn == 21) {
        fight(mutableListOf(Fighter(Ogre), Fighter(Ogre), Fighter(Ogre)))
    }
    if (turn == 23) {
        fight(mutableListOf(Fighter(Golem), Fighter(Golem), Fighter(Golem), Fighter(Golem), Fighter(Golem)))
    }
    if (turn == 25) {
        fight(mutableListOf(Fighter(God)))
    }
    turn++

    val gainedResources = Resources(
            wood=units.filterIsInstance<Lumberjack>().count(),
            iron=units.filterIsInstance<Miner>().count(),
            food=units.filterIsInstance<Farmer>().count()
    )

    for (building in buildings) {
      when (building) {
        House -> {
          if (resources.food > 0) {
            resources = resources.reduce(Worker.cost)
            units.add(Worker)
            observer(WorkerEvent)
          }
        }
      }
    }

    toConvert?.let {
      resources = resources.reduce(it.cost)
      units.remove(Worker)
      units.add(it)
      toConvert = null
      observer(ConvertEvent(it))
    }

    toBuild?.let {
      resources = resources.reduce(it.cost)
      buildings.add(it)
      toBuild = null
      observer(BuildEvent(it))
    }

    resources = resources.augment(gainedResources)
    observer(ResourcesEvent(gainedResources))
  }

  private fun fight(attackers: MutableList<Fighter>) {
    observer(AttackEvent(attackers.map { it.creature }))
    val defenders = units.filter { it.attack > 0 }.plus(units.filter { it.attack == 0 }).map { Fighter(it) }.toMutableList()
    while (defenders.isNotEmpty() && attackers.isNotEmpty()) {

      for (d in defenders) {
        val target = attackers.first()
        target.damage += d.creature.attack
        if (buildings.contains(Tower)) {
          target.damage++
        }
        if (target.damage >= target.creature.defence) {
          attackers.remove(target)
          observer(DeathEvent(target.creature))
        }
        if (attackers.isEmpty()) {
          break
        }
      }

      for (a in attackers) {
        val target = defenders.first()
        target.damage += a.creature.attack
        if (target.damage >= target.creature.defence + if (buildings.contains(Wall)) 1 else 0) {
          defenders.remove(target)
          units.remove(target.creature)
          observer(DeathEvent(target.creature))
        }
        if (defenders.isEmpty()) {
          break
        }
      }

    }
    if (attackers.isEmpty()) {
      observer(WinEvent)
    } else if (defenders.isEmpty()) {
      defeated = true
      observer(LoseEvent)
    }
  }
}
