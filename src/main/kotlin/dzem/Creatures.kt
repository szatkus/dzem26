package dzem

sealed class Creature (
  val cost: Resources,
  val attack: Int,
  val defence: Int,
  val description: String = ""
)

object Worker: Creature(Resources(food=1), 0, 1)
object Lumberjack: Creature(Resources(iron=1), 0, 1, "Gather 1 wood every turn.")
object Farmer: Creature(Resources(iron=1), 0, 1, "Gather 1 food every turn.")
object Miner: Creature(Resources(wood=1), 0, 1, "Gather 1 iron every turn.")
object Militia: Creature(Resources(iron=1, wood=1), 1, 1, "Attack 1; Defence: 1")
object Guardian: Creature(Resources(iron=2, wood=2), 1, 2, "Attack 1; Defence: 2")
object Mage: Creature(Resources(iron=2, wood=2, food=1), 2, 1, "Attack 2; Defence: 1")
object Warrior: Creature(Resources(iron=4, wood=4), 2, 2, "Attack 2; Defence: 2")
object Cavalry: Creature(Resources(iron=4, wood=4, food=4), 3, 3, "Attack 3; Defence: 3")

object Slime: Creature(Resources(), 1, 1)
object Goblin: Creature(Resources(), 2, 2)
object Ogre: Creature(Resources(), 3, 10)
object Golem: Creature(Resources(), 5, 15)
object God: Creature(Resources(), 10, 2000)