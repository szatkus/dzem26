package dzem

fun write(text: String) {
  for (c in text) {
    print(c)
    Thread.sleep(40)
  }
  println()
}

fun displayResources(resources: Resources) {
  write("Food: ${resources.food}")
  write("Wood: ${resources.wood}")
  write("Iron: ${resources.iron}")
}
