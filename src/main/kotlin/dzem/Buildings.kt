package dzem

sealed class Building (
  val cost: Resources,
  val quantity: Int = 1,
  val description: String
)

object House: Building(Resources(wood=1), quantity=4, description = "Produces one Worker every turn.")
object Barracks: Building(Resources(wood=1), description = "Allows to train simple soldiers.")
object Farm: Building(Resources(wood=1), description = "Allows to train Farmers.")
object Mine: Building(Resources(wood=1), description = "Allows to train Miners.")
object Shrine: Building(Resources(wood=2, iron=1), description = "Allows to train Mages.")
object Castle: Building(Resources(iron=3), description = "Allows to train Warriors.")
object Stable: Building(Resources(food=2, wood=2, iron=2), description = "Allows to train Cavalry.")
object Wall: Building(Resources(wood=3, iron=3, food=1), description = "Adds 1 defence point to every creature")
object Tower: Building(Resources(food=3, wood=3, iron=1), description = "Adds 1 attack point to every creature")