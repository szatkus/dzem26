package dzem

class Resources (
  val wood: Int = 0,
  val iron: Int = 0,
  val food: Int = 0
) {
  fun canAfford(resources: Resources) =
    wood >= resources.wood &&
      iron >= resources.iron &&
      food >= resources.food

  fun reduce(resources: Resources?): Resources = if (resources != null) {
    Resources(
      wood = wood - resources.wood,
      iron = iron - resources.iron,
      food = food - resources.food,
    )
  } else {
    this
  }

  fun augment(resources: Resources?): Resources = if (resources != null) {
    Resources(
      wood = wood + resources.wood,
      iron = iron + resources.iron,
      food = food + resources.food,
    )
  } else {
    this
  }

  fun isNegative() =
    wood < 0 ||
      iron < 0 ||
      food < 0
}